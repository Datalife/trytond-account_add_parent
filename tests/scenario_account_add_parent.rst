===========================
Account Add Parent Scenario
===========================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts

Install account_add_parent::

    >>> config = activate_modules('account_add_parent')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = create_fiscalyear(company)
    >>> fiscalyear.click('create_period')
    >>> period = fiscalyear.periods[1]

Create chart of accounts::

    >>> Account = Model.get('account.account')
    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> cash = accounts['cash']

Add parent to cash account::

    >>> parent_id = cash.parent.id
    >>> add_parent = Wizard('account.account.add_parent', [cash])
    >>> cash.reload()
    >>> cash.parent != parent_id
    True
    >>> new_parent = cash.parent
    >>> new_parent.parent.id == parent_id
    True
    >>> bool(new_parent.type)
    False
    >>> bool(cash.type)
    True
    >>> cash.code = 'CASH0'
    >>> cash.save()
    >>> Configuration = Model.get('account.configuration')
    >>> conf = Configuration(1)
    >>> conf.add_parent_code_digits = '0'
    >>> conf.save()
    >>> add_parent = Wizard('account.account.add_parent', [cash])
    >>> cash.reload()
    >>> cash.code
    'CASH00'