# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.wizard import Wizard, StateTransition


class Configuration(metaclass=PoolMeta):
    __name__ = 'account.configuration'

    add_parent_code_digits = fields.Char('Add parent digit',
        help='Digits to append when adding a parent account.')

    @classmethod
    def default_add_parent_code_digits(cls):
        return None


class AddParent(Wizard):
    '''Add account parent'''
    __name__ = 'account.account.add_parent'

    start = StateTransition()

    def values_to_copy(self, account):
        res = {
            'type': None,
            'party_required': False,
            'childs': None
        }
        if (account.code
                and account.parent
                and account.parent.code
                and len(account.code) > len(account.parent.code)):
            res['code'] = account.code[:len(account.parent.code) + 1]
        return res

    def transition_start(self):
        pool = Pool()
        Account = pool.get('account.account')
        Conf = pool.get('account.configuration')

        conf = Conf(1)
        accounts = self.records
        for account in accounts:
            parent_account, = Account.copy([account],
                self.values_to_copy(account))
            account.parent = parent_account
            if account.template and not account.template_override:
                account.template_override = True
            if account.code and conf.add_parent_code_digits is not None:
                account.code += conf.add_parent_code_digits

        Account.save(accounts)
        return 'end'
