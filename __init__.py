# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import account


def register():
    Pool.register(
        account.Configuration,
        module='account_add_parent', type_='model')
    Pool.register(
        account.AddParent,
        module='account_add_parent', type_='wizard')
